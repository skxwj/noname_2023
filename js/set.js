if (!(navigator.userAgent.match(
		/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i
	))) {
	$(".aui-flexView").css({
		"width": '600px',
		"boxShadow": '0px 0px 0px 1px #CACDD1,0px 0px 0px 8px #F1F3F4,0px 0px 4px 8px #333'
	});
}


//轮播图
var mainimgList = [{
		img: "./images/lunbotu/bg9.jpg",
		url: "https://mp.weixin.qq.com/s/uGkvPOeJU4V3etdBulit6w",
	},{
		img: "./images/lunbotu/bg8.jpg",
		url: "https://mp.weixin.qq.com/s/p3-MgEeg0go93XIEKdrmUg",
	}, {
		img: "./images/lunbotu/bg7.jpg",
		url: "https://mp.weixin.qq.com/s/JUA2wlt7na5E8patuAPpww",
	}, {
		img: "./images/lunbotu/bg6.jpg",
		url: "https://mp.weixin.qq.com/s/HX0oXcfX9UAZ26UHzJxYew",
	}, {
		img: "./images/lunbotu/bg5.jpg",
		url: "https://mp.weixin.qq.com/s/HAvtR4tkjV8ZX2akXMoxyg",
	}, {
		img: "./images/lunbotu/bg3.jpg",
		url: "https://mp.weixin.qq.com/s/oGe41zpGN_xLjUmDl6VKVA",
	},
	{
		img: "./images/lunbotu/bg4.jpg",
		url: "https://mp.weixin.qq.com/s/pVexXVhdUCXw6e9ifU00FA",
	},
	{
		img: "./images/lunbotu/bg2.jpg",
		url: "https://mp.weixin.qq.com/s/VWOmHaeqJLQmD_Rez6HfkQ",
	},
	{
		img: "./images/lunbotu/bg1.jpg",
		url: "http://mp.weixin.qq.com/mp/homepage?__biz=MzUwOTMwMjExNQ==&hid=5&sn=b5844538f6f0a49eb2423a900f19d888&scene=18#wechat_redirect", //javascript:;
	}
]

var mainimg = document.getElementsByClassName('main_img')[0]
var imgfont = document.getElementsByClassName('img_font')[0]
for (var i = 0; i < mainimgList.length; i++) {
	mainimg.innerHTML += "<li><a href=" + mainimgList[i].url + "><img style='width: 100%;height:13rem;' src=" +
		mainimgList[i].img + " alt=''></a></li>"
	imgfont.innerHTML += '<span>' + (Number(i) + 1) + '/' + mainimgList.length + '</span>'
}


// 轮播推荐消息
var newsList = [{
		name: "无名杀《北极》拓展包分享",
		url: "https://mp.weixin.qq.com/s/HX0oXcfX9UAZ26UHzJxYew"
	},
	{
		name: "《好名的世界》扩展1.6.5版本更新",
		url: "https://mp.weixin.qq.com/s/loMXC9PFPLTeLHPZ6ymVPQ"
	},
	{
		name: "《新将包》扩展分享推荐",
		url: "https://mp.weixin.qq.com/s/Q22DuOnKwp1mq6SKF_fjXg"
	},
	{
		name: "《奇特物语》扩展1.6版本更新(过年快乐)",
		url: "https://mp.weixin.qq.com/s/kbBWHQ46xJ1wsVqN7OI5Jw"
	},
	{
		name: "《天牢令》扩展23.04.01更新",
		url: "https://mp.weixin.qq.com/s/0aGuNbv9Qfny-zK53tsuPw"
	},
	{
		name: "无名杀《官将重修》扩展1.18更新",
		url: "https://mp.weixin.qq.com/s/vMi8K_Y041RoGIAYsut7jg"
	}
]
var newsLi = document.getElementsByClassName('news_li')[0]
for (var i = 0; i < newsList.length; i++) {
	newsLi.innerHTML += "<li><a href=" + newsList[i].url + ">" + newsList[i].name + "</a></li>"
}


// 推荐QQ群
var qqList = [{
		name: "无名杀《第叁幻界》交",
		url: "https://jq.qq.com/?_wv=1027&k=iYBrLaaO",
		img: "images/qun/8.jpg",
		renshu: "410",
		qunhao: "178323816"
	}, {
		name: "无名杀将灵扩展发布群",
		url: "https://jq.qq.com/?_wv=1027&k=hbxWFDe3",
		img: "images/qun/7.jpg",
		renshu: "394",
		qunhao: "738357530"
	}, {
		name: "方舟：天灾...",
		url: "https://jq.qq.com/?_wv=1027&k=0Bdj5Mt8",
		img: "images/qun/6.jpg",
		renshu: "200",
		qunhao: "926335239"
	},
	{
		name: "无名杀技能...",
		url: "https://jq.qq.com/?_wv=1027&k=314IxwYS",
		img: "images/qun/5.jpg",
		renshu: "200",
		qunhao: "643876193"
	},
	{
		name: "【活动武将】...",
		url: "https://jq.qq.com/?_wv=1027&k=CWLbFaaf",
		img: "images/qun/4.jpg",
		renshu: "138",
		qunhao: "346854437"
	},
	{
		name: "武将界面交流...",
		url: "https://jq.qq.com/?_wv=1027&k=XI2ZLKmG",
		img: "images/qun/1.jpg",
		renshu: "1823",
		qunhao: "834888594"
	},
	{
		name: "无名杀特效测...",
		url: "https://jq.qq.com/?_wv=1027&k=du4ua9hE",
		img: "images/qun/2.jpg",
		renshu: "500",
		qunhao: "288155120"
	},
	{
		name: "無名殺官方群Ⅰ",
		url: "https://jq.qq.com/?_wv=1027&k=zDIx5seq",
		img: "images/qun/noname.jpg",
		renshu: "2000",
		qunhao: "335438250"
	},
	{
		name: "无名杀官方群Ⅱ",
		url: "https://jq.qq.com/?_wv=1027&k=Pdk1cRAv",
		img: "images/qun/noname2.jpg",
		renshu: "1505",
		qunhao: "348943983"
	},
	{
		name: "无名杀官方群Ⅲ",
		url: "https://jq.qq.com/?_wv=1027&k=FUjuVEj5",
		img: "images/qun/noname3.jpg",
		renshu: "1632",
		qunhao: "459406548"
	},
	{
		name: "无名杀官方群Ⅴ",
		url: "https://jq.qq.com/?_wv=1027&k=6AORijG8",
		img: "images/qun/noname4.jpg",
		renshu: "1531",
		qunhao: "518258435"
	},
	{
		name: "无名杀官方群Ⅵ",
		url: "https://jq.qq.com/?_wv=1027&k=lk5NJkW0",
		img: "images/qun/noname6.jpg",
		renshu: "1999",
		qunhao: "536979725"
	},
	{
		name: "无名杀官方群Ⅶ",
		url: "https://jq.qq.com/?_wv=1027&k=qGOQlj6T",
		img: "images/qun/noname7.jpg",
		renshu: "1725",
		qunhao: "1080968725"
	},
	{
		name: "无名杀官方群Ⅷ",
		url: "https://jq.qq.com/?_wv=1027&k=vBGutFiU",
		img: "images/qun/noname8.jpg",
		renshu: "1893",
		qunhao: "1038572358"
	},
	{
		name: "无名杀官方群Ⅸ",
		url: "https://jq.qq.com/?_wv=1027&k=d1TYSKpj",
		img: "images/qun/noname9.jpg",
		renshu: "1971",
		qunhao: "292753743"
	},
	{
		name: "无名杀官方群Ⅹ",
		url: "https://jq.qq.com/?_wv=1027&k=VH4hX5VX",
		img: "images/qun/noname.jpg",
		renshu: "2423",
		qunhao: "784051469"
	},
	{
		name: "十周年UI（手...",
		url: "https://jq.qq.com/?_wv=1027&k=QGTOZ0id",
		img: "images/qun/ui.jpg",
		renshu: "899",
		qunhao: "421537580"
	},
	{
		name: "无名杀玄武...",
		url: "https://jq.qq.com/?_wv=1027&k=b0cBTIdn",
		img: "images/qun/xw.jpg",
		renshu: "1987",
		qunhao: "522136249"
	},
	{
		name: "无名杀金庸...",
		url: "https://jq.qq.com/?_wv=1027&k=3oIvkguA",
		img: "images/qun/jy.jpg",
		renshu: "467",
		qunhao: "697310426"
	},
	{
		name: "无名杀–秦时...",
		url: "https://jq.qq.com/?_wv=1027&k=Es1Spigs",
		img: "images/qun/qinshi.jpg",
		renshu: "94",
		qunhao: "616483541"
	},
]
var qq = document.getElementsByClassName("aui-slide-item-list")[0]
for (var i = 0; i < qqList.length; i++) {
	qq.innerHTML += '<li class="aui-slide-item-item"><a href=' + qqList[i].url + ' class="aui-link"><img src=' +
		qqList[i].img + ' alt=""></a><div class="aui-good-content"><h3>' + qqList[i].name + '</h3><p>' + qqList[i]
		.renshu + '人</p><span>群号:' + qqList[i].qunhao + '</span></div></li>'
}


// 热门扩展
var auiPatternList = [{
		name: "玄武江湖",
		url: "https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzUwOTMwMjExNQ==&action=getalbum&album_id=1712682190653718529#wechat_redirect"
	},
	{
		name: "天牢令",
		url: "https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzUwOTMwMjExNQ==&action=getalbum&album_id=2747481496034492419&scene=173&from_msgid=2247540215&from_itemidx=2&count=3&nolastread=1#wechat_redirect"
	},
	{
		name: "十周年ui",
		url: "https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzUwOTMwMjExNQ==&action=getalbum&album_id=2171834109554229250#wechat_redirect"
	},
	{
		name: "活动武将",
		url: "https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzUwOTMwMjExNQ==&action=getalbum&album_id=2488097605764530178#wechat_redirect"
	},
	{
		name: "	Thunder",
		url: "https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzUwOTMwMjExNQ==&action=getalbum&album_id=2489581202224726019#wechat_redirect"
	},
	{
		name: "RE：英雄杀",
		url: "https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzUwOTMwMjExNQ==&action=getalbum&album_id=2171841663814500356#wechat_redirect"
	},
	{
		name: "千幻聆音",
		url: "https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzUwOTMwMjExNQ==&action=getalbum&album_id=2171840466793693185#wechat_redirect"
	},
	{
		name: "福瑞拓展",
		url: "https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzUwOTMwMjExNQ==&action=getalbum&album_id=2826592095296831491&scene=173&from_msgid=2247539581&from_itemidx=1&count=3&nolastread=1#wechat_redirect"
	},
	{
		name: "特效测试",
		url: "https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzUwOTMwMjExNQ==&action=getalbum&album_id=2171843384468996104#wechat_redirect"
	},
	{
		name: "极略自用",
		url: "https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzUwOTMwMjExNQ==&action=getalbum&album_id=2171847276984434690&scene=173&from_msgid=2247535003&from_itemidx=6&count=3&nolastread=1#wechat_redirect"
	},
	{
		name: "阳光包",
		url: "https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzUwOTMwMjExNQ==&action=getalbum&album_id=2295896772973707267#wechat_redirect"
	},
	{
		name: "官将重修",
		url: "https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzUwOTMwMjExNQ==&action=getalbum&album_id=2790939328352108545&scene=173&from_msgid=2247539123&from_itemidx=3&count=3&nolastread=1#wechat_redirect"
	}
]
var auiPattern = document.getElementsByClassName('aui-pattern')[0]
for (var j = 0; j < auiPatternList.length; j++) {
	auiPattern.innerHTML += "<a href=" + auiPatternList[j].url + ">" + auiPatternList[j].name + "</a>"
}


//资讯广场
var zixun = document.getElementsByClassName('zixun')[0]
zixun.onclick = function() {

	// var broser = window.navigator.userAgent.toLowerCase();
	// if (broser.match(/MicroMessenger/i) == 'micromessenger') {
	// 	alert('请选择"在浏览器打开",再进入"资讯广场"')
	// } else {
	//window.location.href = 'https://static-64bbfebf-7fd8-4fcb-9faf-c05bd62be4ed.bspapp.com/#/';
	// window.location.href = 'https://static-mp-6f74070b-8ba1-480f-b0d7-2d88c0be884c.next.bspapp.com/#/';
	window.location.href = 'wenzhang/zixun/index.html'
	// }

}
