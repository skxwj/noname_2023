![RUNOOB 图标](https://qinkunwei.gitee.io/noname/images/code.jpg)

## [👉点击进入《noname无名杀网站》✔️](https://qinkunwei.gitee.io/noname/)
<br>

#### 声明

    无名杀创始人为“水乎”，无名杀为开源免费游戏，
    谨防受骗! ! !
    游戏开源，仅供个人学习，研究之用，
    请勿用于商业用途。

#### 微信公众号"无名杀扩展交流"

    欢迎关注微信公众号“无名杀扩展交流”，
    这里有最新的无名杀资讯，以及各种扩展试玩分享。
    帮大家找到最好玩的扩展

#### 信息栏

|  中文名	| 无名杀	|
|  ----		| ----		|
| 别名		| 無名殺	|
| 英文名	| noname	|
| 制作人	| 水乎		|
| 现更新者	| 苏婆玛丽奥|

#### 无名杀简介

    无名杀于2013年底发布，
    是一款基于html,css,js开发的h5卡牌游戏，
    完全免费的开源游戏，开发者：水乎，
    由‘苏婆玛丽奥’继续维护源码。
    供个人学习，研究之用，勿用于商业用途。
    武将包含三国杀，英雄杀，炉石，古剑奇谭，仙剑等，
    模式包含战棋、塔防，炉石，身份，挑战等众多玩法。
    拥有智能AI可以实现联机和单机的两种游戏方式，
    并能通过扩展功能，diy设计与分享。

#### 帮助相关

1. [无名杀百度云盘下载链接](https://pan.baidu.com/s/1DeVMVn9Uy1IdPX8kT60Y7g)
2. [无名杀代码教程网站，学习代码，自制武将](https://qinkunwei.gitee.io/ciyage/archives/)
3. [无名杀版本更新公告地址，查看版本更新内容](https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzUwOTMwMjExNQ==&action=getalbum&album_id=1872742397336453125&uin=&key=&devicetype=Windows+10+x64&version=6303051e&lang=zh_CN&ascene=7&fontgear=2) 
4. [无名杀网页版，网页版不同步更新](https://qinkunwei.github.io/nonamekill/) 
5. [官方群及相关群，方便交流经验](https://mp.weixin.qq.com/s/QdScl3CX34YirnqLh8IGew)