(window["webpackJsonp"] = window["webpackJsonp"] || []).push([
	["pages-tabbar-index-index", "pages-tabbar-fenlei-fenlei", "pages-tabbar-find-find",
		"pages-tabbar-home-home", "pages-tabbar-my-my"
	], {
		"01b6": function(t, a, i) {
			"use strict";
			(function(t) {
				i("7a82"), Object.defineProperty(a, "__esModule", {
					value: !0
				}), a.default = void 0, i("99af");
				var n = {
					data: function() {
						return {
							chakanIndex: -1,
							newsList: {
								data: [],
								isLoading: 1,
								num: 0
							},
							sousuoText: "",
							sousuo: "",
							widthWX: !1
						}
					},
					mounted: function() {
						this.getData()
					},
					methods: {
						goUrl: function(t) {
							window.location.href = t
						},
						goSousuo: function() {
							this.sousuo = this.sousuoText, this.newsList = {
								data: [],
								isLoading: 1,
								num: 0
							}, this.getData()
						},
						getData: function() {
							var a = this; - 1 != this.newsList.isLoading && (uni.showLoading({
								title: "加载中"
							}), t.callFunction({
								name: "kuozhanList",
								data: {
									pageSize: 10,
									page: this.newsList.num,
									sousuo: this.sousuo
								}
							}).then((function(t) {
								uni.hideLoading(), 0 == a.newsList.num ? a
									.newsList.data = t.result.data : a.newsList
									.data = a.newsList.data.concat(t.result
										.data), a.newsList.num++, t.result.data
									.length <= 9 && (a.newsList.isLoading = -1),
									a.$forceUpdate()
							})))
						},
						goChakan: function(t) {
							this.chakanIndex = t
						}
					}
				};
				a.default = n
			}).call(this, i("a9ff")["default"])
		},
		"0b64": function(t, a, i) {
			"use strict";
			i.r(a);
			var n = i("741f"),
				e = i.n(n);
			for (var o in n)["default"].indexOf(o) < 0 && function(t) {
				i.d(a, t, (function() {
					return n[t]
				}))
			}(o);
			a["default"] = e.a
		},
		1110: function(t, a, i) {
			"use strict";
			i.r(a);
			var n = i("f700"),
				e = i.n(n);
			for (var o in n)["default"].indexOf(o) < 0 && function(t) {
				i.d(a, t, (function() {
					return n[t]
				}))
			}(o);
			a["default"] = e.a
		},
		"13ed": function(t, a, i) {
			"use strict";
			i.d(a, "b", (function() {
				return n
			})), i.d(a, "c", (function() {
				return e
			})), i.d(a, "a", (function() {}));
			var n = function() {
					var t = this,
						a = t.$createElement,
						i = t._self._c || a;
					return i("v-uni-view", [i("v-uni-view", {
						staticClass: "tab-bar"
					}, [i("v-uni-view", {
						staticClass: "cu-bar search bg-white",
						class: {
							widthWX: t.widthWX
						}
					}, [i("v-uni-view", {
						staticClass: "search-form round"
					}, [i("v-uni-text", {
						staticClass: "cuIcon-search"
					}), i("v-uni-input", {
						attrs: {
							type: "text",
							placeholder: "请输入文章标题",
							"confirm-type": "search"
						},
						model: {
							value: t.sousuoText,
							callback: function(a) {
								t.sousuoText = a
							},
							expression: "sousuoText"
						}
					})], 1), i("v-uni-view", {
						staticClass: "action"
					}, [i("v-uni-button", {
						staticClass: "cu-btn bg-blue round",
						on: {
							click: function(a) {
								arguments[0] = a = t
									.$handleEvent(a), t.goSousuo
									.apply(void 0, arguments)
							}
						}
					}, [t._v("搜索")])], 1)], 1), i("v-uni-scroll-view", {
						staticClass: "scroll-h",
						attrs: {
							"scroll-x": !0,
							"show-scrollbar": !1,
							"scroll-with-animation": !0,
							"scroll-into-view": t.scrollInto
						}
					}, t._l(t.tabBars, (function(a, n) {
						return i("v-uni-view", {
							key: a.value,
							staticClass: "uni-tab-item",
							attrs: {
								id: "index" + a.value,
								"data-current": n
							},
							on: {
								click: function(a) {
									arguments[0] = a = t
										.$handleEvent(a), t
										.ontabtap.apply(void 0,
											arguments)
								}
							}
						}, [i("v-uni-text", {
							staticClass: "uni-tab-item-title",
							class: t.tabIndex == n ?
								"uni-tab-item-title-active" :
								""
						}, [t._v(t._s(a.text))])], 1)
					})), 1)], 1), i("v-uni-swiper", {
						staticClass: "box-ul",
						attrs: {
							current: t.tabIndex,
							duration: 300
						},
						on: {
							change: function(a) {
								arguments[0] = a = t.$handleEvent(a), t.ontabchange
									.apply(void 0, arguments)
							}
						}
					}, t._l(t.newsList, (function(a, n) {
						return i("v-uni-swiper-item", {
							key: n,
							staticClass: "box-ul"
						}, [i("v-uni-scroll-view", {
							staticClass: "box-li",
							attrs: {
								"scroll-y": "true"
							},
							on: {
								scrolltolower: function(a) {
									arguments[0] = a = t
										.$handleEvent(a), t
										.loadMore(n)
								}
							}
						}, [i("v-uni-view", {
							staticClass: "list"
						}, [t._l(t.newsList, (function(a,
						n) {
							return [t._l(a.data,
									(function(
										a,
										e
										) {
										return n ==
											t
											.tabIndex ?
											[i("v-uni-view", {
													key: e +
														"_0",
													staticClass: "list-li",
													on: {
														click: function(
															i
															) {
															arguments
																[
																	0] =
																i =
																t
																.$handleEvent(
																	i
																	),
																t
																.goRouter(
																	1,
																	"/pages/tabbar/home/text?id=" +
																	a
																	._id
																	)
														}
													}
												},
												[i("v-uni-image", {
														staticClass: "fengmian",
														attrs: {
															src: a
																.img ||
																"/static/logo.png",
															mode: "aspectFill"
														}
													}),
													i("v-uni-view", {
															staticClass: "text"
														},
														[i("v-uni-view", {
																	staticClass: "title"
																},
																[t._v(t._s(a.title ||
																	""
																	))]
																),
															i("v-uni-view", {
																	staticClass: "info"
																},
																[i("v-uni-view",
																		[t._v(t._s(a.zuozhe ||
																			""
																			))]
																		),
																	i("v-uni-view",
																		[t._v(t._s(a.time ||
																			""
																			))]
																		)
																],
																1
																)
														],
														1
														)
												],
												1
												)] :
											t
											._e()
									})),
								n == t
								.tabIndex ?
								i("v-uni-view", {
									key: n +
										"_1",
									staticClass: "box-null"
								}, [0 ==
									a
									.data
									.length ?
									i("v-uni-view", {
											staticClass: "bg"
										},
										[i("v-uni-image", {
												attrs: {
													src: "/static/bg.jpg",
													mode: "aspectFit"
												}
											}),
											i("v-uni-view",
												[t._v(
													"暂无数据")]
												)
										],
										1
										) :
									i("v-uni-view", {
										staticClass: "cu-load text-gray",
										class:
											-
											1 ==
											a
											.isLoading ?
											"over" :
											"loading"
									})
								], 1) : t
								._e()
							]
						}))], 2)], 1)], 1)
					})), 1)], 1)
				},
				e = []
		},
		"17f8": function(t, a, i) {
			"use strict";
			i.d(a, "b", (function() {
				return n
			})), i.d(a, "c", (function() {
				return e
			})), i.d(a, "a", (function() {}));
			var n = function() {
					var t = this,
						a = t.$createElement,
						i = t._self._c || a;
					return i("v-uni-view", [i("v-uni-view", {
						class: "pc" == t.shebei ? "content" : ""
					}, [1 === t.page ? i("my", {
						ref: "ismy"
					}) : t._e(), 2 === t.page ? i("home", {
						ref: "ishome"
					}) : t._e(), 3 === t.page ? i("fenlei", {
						ref: "isfenlei"
					}) : t._e(), 4 === t.page ? i("find", {
						ref: "isfind"
					}) : t._e()], 1), i("v-uni-view", {
						class: "pc" == t.shebei ? "tabbar-pc" : "cu-bar tabbar bg-white"
					}, [i("v-uni-view", {
						class: "1" == t.page ? "action text-blue" :
							"action text-gray",
						on: {
							click: function(a) {
								arguments[0] = a = t.$handleEvent(a), t
									.changePage(1)
							}
						}
					}, [i("v-uni-view", {
						staticClass: "cuIcon-text"
					}), t._v("推荐")], 1), i("v-uni-view", {
						class: "2" == t.page ? "action text-blue" :
							"action text-gray",
						on: {
							click: function(a) {
								arguments[0] = a = t.$handleEvent(a), t
									.changePage(2)
							}
						}
					}, [i("v-uni-view", {
						staticClass: "cuIcon-form"
					}), t._v("热文")], 1), i("v-uni-view", {
						staticClass: "action text-gray add-action",
						on: {
							click: function(a) {
								arguments[0] = a = t.$handleEvent(a), t.gofabu
									.apply(void 0, arguments)
							}
						}
					}, [i("v-uni-image", {
						staticClass: "noname",
						attrs: {
							src: "/static/noname.png",
							mode: "scaleToFill"
						}
					}), t._v("noname")], 1), i("v-uni-view", {
						class: "3" == t.page ? "action text-blue" :
							"action text-gray",
						on: {
							click: function(a) {
								arguments[0] = a = t.$handleEvent(a), t
									.changePage(3)
							}
						}
					}, [i("v-uni-view", {
						staticClass: "cuIcon-goodsnew"
					}), t._v("扩展")], 1), i("v-uni-view", {
						class: "4" == t.page ? "action text-blue" :
							"action text-gray",
						on: {
							click: function(a) {
								arguments[0] = a = t.$handleEvent(a), t
									.changePage(4)
							}
						}
					}, [i("v-uni-view", {
						staticClass: "cuIcon-mail"
					}), t._v("公告")], 1)], 1)], 1)
				},
				e = []
		},
		"30ca": function(t, a, i) {
			var n = i("ad1c");
			n.__esModule && (n = n.default), "string" === typeof n && (n = [
				[t.i, n, ""]
			]), n.locals && (t.exports = n.locals);
			var e = i("4f06").default;
			e("4366051b", n, !0, {
				sourceMap: !1,
				shadowMode: !1
			})
		},
		"3e78": function(t, a, i) {
			var n = i("24fb");
			a = n(!1), a.push([t.i,
				".widthWX[data-v-6c32742c]{width:%?550?%}.tab-bar[data-v-6c32742c]{width:100%;position:fixed;top:0;left:0;background-color:#fff;z-index:10;padding-top:calc(0px + %?18?%)}.scroll-h[data-v-6c32742c]{background-color:#fff;width:%?750?%;width:100%;height:%?80?%;flex-direction:row;white-space:nowrap}.uni-tab-item[data-v-6c32742c]{display:inline-block;flex-wrap:nowrap;padding:0 %?68?%}.uni-tab-item-title[data-v-6c32742c]{color:#555;font-size:%?28?%;height:%?80?%;line-height:%?80?%;flex-wrap:nowrap;white-space:nowrap}.uni-tab-item-title-active[data-v-6c32742c]{color:#409eff;padding-bottom:%?20?%;border-bottom:%?4?% solid #409eff}.box-ul[data-v-6c32742c]{min-height:100vh;width:100%}.box-ul .box-li[data-v-6c32742c]{\n  /* swiper内容高度 */height:100%;width:100%}.list[data-v-6c32742c]{padding:calc(0px + %?215?%) %?20?% %?200?%}.list .list-li[data-v-6c32742c]{display:flex;justify-content:space-between;padding:%?20?%;background-color:#fff;border-radius:%?10?%;margin-bottom:%?20?%;box-shadow:%?5?% %?5?% %?8?% #ccc}.list .list-li .fengmian[data-v-6c32742c]{width:%?150?%;height:%?150?%;border-radius:%?10?%}.list .list-li .text[data-v-6c32742c]{padding-left:%?20?%;display:flex;flex-direction:column;justify-content:space-between;width:77%}.list .list-li .text .title[data-v-6c32742c]{color:#333;font-size:%?30?%;white-space:normal;word-break:break-all;display:-webkit-box;-webkit-box-orient:vertical;-webkit-line-clamp:3;overflow:hidden}.list .list-li .text .info[data-v-6c32742c]{color:#9a9a9a;font-size:%?26?%;display:flex;align-items:center;justify-content:space-between;height:%?40?%;line-height:%?40?%}.list .list-li .text .info uni-view[data-v-6c32742c]{width:50%;overflow:hidden;text-overflow:ellipsis;white-space:nowrap}.box-null[data-v-6c32742c]{width:100%;text-align:center}.box-null .bg[data-v-6c32742c]{background-color:#fff;border-radius:%?10?%;padding:%?80?%;box-shadow:%?5?% %?5?% %?8?% #ccc;font-size:%?40?%;font-weight:700;color:#333}.box-null .bg uni-image[data-v-6c32742c]{height:%?350?%}",
				""
			]), t.exports = a
		},
		4049: function(t, a, i) {
			var n = i("24fb");
			a = n(!1), a.push([t.i,
				".app[data-v-17ada069]{padding:%?20?% %?10?% %?200?%}.tuijian-ul[data-v-17ada069]{display:flex;flex-wrap:wrap}.tuijian-ul .tuijian-li[data-v-17ada069]{position:relative;margin:0 %?20?% %?20?%;height:%?320?%;width:%?320?%;background-color:#fff;box-shadow:%?4?% %?8?% %?8?% #ccc;border-radius:%?20?%;overflow:hidden;border:%?10?% solid #fff}.tuijian-ul .tuijian-li .img[data-v-17ada069]{width:100%;height:%?320?%;overflow:hidden}.tuijian-ul .tuijian-li .text[data-v-17ada069]{display:-webkit-box;-webkit-box-orient:vertical;-webkit-line-clamp:2;overflow:hidden;position:absolute;bottom:%?0?%;background-color:#333;color:#fff;padding:%?8?% %?20?%;opacity:.7;width:100%}.box-null[data-v-17ada069]{width:100%;text-align:center}.box-null .bg[data-v-17ada069]{background-color:#fff;border-radius:%?10?%;padding:%?80?%;box-shadow:%?5?% %?5?% %?8?% #ccc;font-size:%?40?%;font-weight:700;color:#333}.box-null .bg uni-image[data-v-17ada069]{height:%?350?%}",
				""
			]), t.exports = a
		},
		"41ee": function(t, a, i) {
			"use strict";
			i.r(a);
			var n = i("17f8"),
				e = i("adda");
			for (var o in e)["default"].indexOf(o) < 0 && function(t) {
				i.d(a, t, (function() {
					return e[t]
				}))
			}(o);
			i("c89b");
			var s = i("f0c5"),
				c = Object(s["a"])(e["default"], n["b"], n["c"], !1, null, "6fbf20fe", null, !1, n["a"],
					void 0);
			a["default"] = c.exports
		},
		"55a2": function(t, a, i) {
			"use strict";
			i.d(a, "b", (function() {
				return n
			})), i.d(a, "c", (function() {
				return e
			})), i.d(a, "a", (function() {}));
			var n = function() {
					var t = this,
						a = t.$createElement,
						i = t._self._c || a;
					return i("v-uni-view", [i("v-uni-view", {
						staticClass: "tab-bar"
					}, [i("v-uni-view", {
						staticClass: "cu-bar search bg-white",
						class: {
							widthWX: t.widthWX
						}
					}, [i("v-uni-view", {
						staticClass: "search-form round"
					}, [i("v-uni-text", {
						staticClass: "cuIcon-search"
					}), i("v-uni-input", {
						attrs: {
							type: "text",
							placeholder: "请输入扩展名称",
							"confirm-type": "search"
						},
						model: {
							value: t.sousuoText,
							callback: function(a) {
								t.sousuoText = a
							},
							expression: "sousuoText"
						}
					})], 1), i("v-uni-view", {
						staticClass: "action"
					}, [i("v-uni-button", {
						staticClass: "cu-btn bg-blue round",
						on: {
							click: function(a) {
								arguments[0] = a = t
									.$handleEvent(a), t.goSousuo
									.apply(void 0, arguments)
							}
						}
					}, [t._v("搜索")])], 1)], 1)], 1), i("v-uni-view", {
						staticClass: "kuozhan-ul"
					}, [t._l(t.newsList.data, (function(a, n) {
						return [i("v-uni-view", {
							key: n + "_0",
							staticClass: "kuozhan-li"
						}, [i("v-uni-view", {
							staticClass: "kuozhan-title"
						}, [i("v-uni-view", [t._v(t._s(a
							.title || ""))]), i(
							"v-uni-view", {
								on: {
									click: function(i) {
										arguments[
											0] = i =
											t
											.$handleEvent(
												i),
											t.goUrl(
												a
												.url
												)
									}
								}
							}, [t._v("下载链接")])], 1), i(
							"v-uni-view", {
								staticClass: "kuozhan-zuozhe"
							}, [i("v-uni-view", [t._v("作者：" + t
									._s(a.zuozhe || "")
									)]), i("v-uni-view", [t
								._v(t._s(a.daxiao ||
									""))
							])], 1), i("v-uni-view", {
							class: [t.chakanIndex != n ?
								"kuozhan-content" :
								"kuozhan-content-child"
							],
							on: {
								click: function(a) {
									arguments[0] = a = t
										.$handleEvent(
										a), t.goChakan(
											n)
								}
							}
						}, [i("v-uni-view", {
							domProps: {
								innerHTML: t._s(a
									.jianjie ||
									"")
							}
						})], 1)], 1)]
					})), i("v-uni-view", {
						staticClass: "box-null"
					}, [0 == t.newsList.data.length ? i("v-uni-view", {
						staticClass: "bg"
					}, [i("v-uni-image", {
						attrs: {
							src: "/static/bg.jpg",
							mode: "aspectFit"
						}
					}), i("v-uni-view", [t._v("暂无数据")])], 1) : i("v-uni-view", {
						staticClass: "cu-load text-gray",
						class: -1 == t.newsList.isLoading ? "over" :
							"loading"
					})], 1)], 2)], 1)
				},
				e = []
		},
		5774: function(t, a, i) {
			"use strict";
			i.r(a);
			var n = i("fe86"),
				e = i("1110");
			for (var o in e)["default"].indexOf(o) < 0 && function(t) {
				i.d(a, t, (function() {
					return e[t]
				}))
			}(o);
			i("f148");
			var s = i("f0c5"),
				c = Object(s["a"])(e["default"], n["b"], n["c"], !1, null, "4061ef9a", null, !1, n["a"],
					void 0);
			a["default"] = c.exports
		},
		"64dd": function(t, a, i) {
			"use strict";
			i.r(a);
			var n = i("55a2"),
				e = i("b5f8");
			for (var o in e)["default"].indexOf(o) < 0 && function(t) {
				i.d(a, t, (function() {
					return e[t]
				}))
			}(o);
			i("e6ef");
			var s = i("f0c5"),
				c = Object(s["a"])(e["default"], n["b"], n["c"], !1, null, "003cd546", null, !1, n["a"],
					void 0);
			a["default"] = c.exports
		},
		"66dd": function(t, a, i) {
			var n = i("4049");
			n.__esModule && (n = n.default), "string" === typeof n && (n = [
				[t.i, n, ""]
			]), n.locals && (t.exports = n.locals);
			var e = i("4f06").default;
			e("7970b672", n, !0, {
				sourceMap: !1,
				shadowMode: !1
			})
		},
		"741f": function(t, a, i) {
			"use strict";
			(function(t) {
				i("7a82"), Object.defineProperty(a, "__esModule", {
					value: !0
				}), a.default = void 0, i("d3b7"), i("159b"), i("14d9"), i("e9c4"), i("99af");
				var n = {
					data: function() {
						return {
							tabIndex: 0,
							tabBars: [],
							scrollInto: "",
							newsList: [],
							sousuoText: "",
							sousuo: "",
							widthWX: !1
						}
					},
					mounted: function() {
						var a = this;
						t.callFunction({
							name: "getwenzhangType"
						}).then((function(t) {
							a.tabBars = t.result, a.tabBars.forEach((function(t) {
								a.newsList.push({
									data: [],
									isLoading: 1,
									num: 0
								})
							})), a.getList(0)
						}))
					},
					methods: {
						goRouter: function(t, a) {
							this.$_router(t, a)
						},
						ontabchange: function(t) {
							var a = t.target.current || t.detail.current;
							this.switchTab(a)
						},
						ontabtap: function(t) {
							var a = t.target.dataset.current || t.currentTarget.dataset.current;
							this.switchTab(a)
						},
						switchTab: function(t) {
							this.tabIndex !== t && (this.tabIndex = t, this.scrollInto =
								"index" + this.tabBars[t].id, this.sousuo = "", this
								.sousuoText = "", this.newsList[this.tabIndex] = {
									data: [],
									isLoading: 1,
									num: 0
								}, this.getList())
						},
						loadMore: function(t) {
							this.getList()
						},
						goSousuo: function() {
							this.sousuo = this.sousuoText, this.newsList[this.tabIndex] = {
								data: [],
								isLoading: 1,
								num: 0
							}, this.getList()
						},
						getList: function(a) {
							var i = this;
							JSON.stringify(a) || (a = this.tabIndex), -1 != this.newsList[a]
								.isLoading && (uni.showLoading({
									title: "加载中"
								}), t.callFunction({
									name: "getwenzhangList",
									data: {
										leixing: a + 1,
										pageSize: 10,
										page: this.newsList[a].num,
										sousuo: this.sousuo
									}
								}).then((function(t) {
									uni.hideLoading(), 0 == i.newsList[a].num ? i
										.newsList[a].data = t.result.data : i
										.newsList[a].data = i.newsList[a].data
										.concat(t.result.data), i.newsList[a].num++,
										t.result.data.length <= 9 && (i.newsList[a]
											.isLoading = -1), i.$forceUpdate()
								})))
						}
					}
				};
				a.default = n
			}).call(this, i("a9ff")["default"])
		},
		"7ff5": function(t, a, i) {
			"use strict";
			(function(t) {
				i("7a82");
				var n = i("4ea4").default;
				Object.defineProperty(a, "__esModule", {
					value: !0
				}), a.default = void 0, i("ac1f"), i("466d");
				var e = n(i("c20e")),
					o = n(i("d99f")),
					s = n(i("64dd")),
					c = n(i("5774"));
				if (navigator.userAgent.match(
						/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i
						)) var u = "mobile";
				else u = "pc";
				var l = {
					components: {
						my: e.default,
						home: o.default,
						fenlei: s.default,
						find: c.default
					},
					data: function() {
						return {
							page: 1,
							shebei: u
						}
					},
					onLoad: function() {},
					methods: {
						changePage: function(t) {
							this.page = t
						},
						gofabu: function() {
							window.location.href = "https://qinkunwei.github.io/nonamekill/"
						},
						tishi: function() {
							uni.showModal({
								title: "提示",
								content: '"资讯广场"服务器不稳定，想要了解更多资讯，请点击"复制链接"并在微信内粘贴打开',
								confirmText: "复制链接",
								success: function(a) {
									a.confirm ? (uni.setClipboardData({
										data: "https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=MzUwOTMwMjExNQ==&scene=124#wechat_redirect",
										success: function() {
											uni.showToast({
												title: "复制成功",
												duration: 2e3
											})
										}
									}), setTimeout((function() {
										window.location.href =
											"weixin://"
									}), 500)) : a.cancel && t("log", "用户点击取消",
										" at pages/tabbar/index/index.vue:103")
								}
							})
						}
					},
					onReachBottom: function() {
						1 == this.page ? this.$refs.ismy.getData() : 3 == this.page && this
							.$refs.isfenlei.getData()
					}
				};
				a.default = l
			}).call(this, i("0de9")["log"])
		},
		"8c2c": function(t, a, i) {
			var n = i("3e78");
			n.__esModule && (n = n.default), "string" === typeof n && (n = [
				[t.i, n, ""]
			]), n.locals && (t.exports = n.locals);
			var e = i("4f06").default;
			e("719cedf3", n, !0, {
				sourceMap: !1,
				shadowMode: !1
			})
		},
		"94ba": function(t, a, i) {
			var n = i("24fb");
			a = n(!1), a.push([t.i,
				".title[data-v-4061ef9a]{font-size:%?28?%;color:#a5a5a5;padding:%?20?%;text-align:center}.content[data-v-4061ef9a]{margin:%?20?%;padding:%?20?%;background-color:#fff;border-radius:%?10?%;font-size:%?25?%;color:#333;box-shadow:%?5?% %?5?% %?8?% #ccc;white-space:normal;word-break:break-all}.guanli[data-v-4061ef9a]{position:absolute;right:%?20?%;display:flex;align-items:center}.guanli .cuIcon-group_fill[data-v-4061ef9a]{font-size:%?40?%;margin-right:%?10?%}",
				""
			]), t.exports = a
		},
		a37d: function(t, a, i) {
			"use strict";
			(function(t) {
				i("7a82"), Object.defineProperty(a, "__esModule", {
					value: !0
				}), a.default = void 0, i("99af");
				var n = {
					data: function() {
						return {
							newsList: {
								data: [],
								isLoading: 1,
								num: 0
							}
						}
					},
					mounted: function() {
						this.getData()
					},
					methods: {
						goUrl: function(t) {
							1 == t.range ? (uni.setStorageSync("web", t), this.$_router(1,
								"/pages/tabbar/my/web")) : 2 == t.range && (window.location
								.href = t.url)
						},
						getData: function() {
							var a = this; - 1 != this.newsList.isLoading && (uni.showLoading({
								title: "加载中"
							}), t.callFunction({
								name: "tuijianList",
								data: {
									pageSize: 10,
									page: this.newsList.num,
									sousuo: ""
								}
							}).then((function(t) {
								uni.hideLoading(), 0 == a.newsList.num ? a
									.newsList.data = t.result.data : a.newsList
									.data = a.newsList.data.concat(t.result
										.data), a.newsList.num++, t.result.data
									.length <= 9 && (a.newsList.isLoading = -1),
									a.$forceUpdate()
							})))
						}
					}
				};
				a.default = n
			}).call(this, i("a9ff")["default"])
		},
		ad1c: function(t, a, i) {
			var n = i("24fb");
			a = n(!1), a.push([t.i,
				".tabbar[data-v-6fbf20fe]{width:100%;position:fixed;bottom:0;left:0;box-shadow:0 0 %?20?% #ddd}.tabbar .icon[data-v-6fbf20fe]{width:%?50?%;height:%?50?%}.hide[data-v-6fbf20fe]{display:none}.noname[data-v-6fbf20fe]{height:%?75?%;width:%?75?%;position:absolute;top:%?-40?%;left:0;right:0;margin:0 auto;border-radius:50%;z-index:100;box-shadow:%?2?% %?8?% %?10?% #ccc}.content[data-v-6fbf20fe]{width:50%}.tabbar-pc[data-v-6fbf20fe]{width:20%;min-height:100%;position:fixed;top:0;right:20%;box-shadow:%?10?% %?10?% %?10?% #ccc;background-color:#fff}.tabbar-pc .action[data-v-6fbf20fe]:nth-child(1){margin-top:calc(0px + %?300?%)}.tabbar-pc .action[data-v-6fbf20fe]{display:flex;align-items:center;justify-content:center;margin-top:%?100?%}.tabbar-pc .action uni-view[data-v-6fbf20fe]{margin-right:%?10?%}",
				""
			]), t.exports = a
		},
		adda: function(t, a, i) {
			"use strict";
			i.r(a);
			var n = i("7ff5"),
				e = i.n(n);
			for (var o in n)["default"].indexOf(o) < 0 && function(t) {
				i.d(a, t, (function() {
					return n[t]
				}))
			}(o);
			a["default"] = e.a
		},
		b5f8: function(t, a, i) {
			"use strict";
			i.r(a);
			var n = i("01b6"),
				e = i.n(n);
			for (var o in n)["default"].indexOf(o) < 0 && function(t) {
				i.d(a, t, (function() {
					return n[t]
				}))
			}(o);
			a["default"] = e.a
		},
		b828: function(t, a, i) {
			"use strict";
			var n = i("8c2c"),
				e = i.n(n);
			e.a
		},
		c20e: function(t, a, i) {
			"use strict";
			i.r(a);
			var n = i("e037"),
				e = i("d80a");
			for (var o in e)["default"].indexOf(o) < 0 && function(t) {
				i.d(a, t, (function() {
					return e[t]
				}))
			}(o);
			i("ff2a");
			var s = i("f0c5"),
				c = Object(s["a"])(e["default"], n["b"], n["c"], !1, null, "17ada069", null, !1, n["a"],
					void 0);
			a["default"] = c.exports
		},
		c89b: function(t, a, i) {
			"use strict";
			var n = i("30ca"),
				e = i.n(n);
			e.a
		},
		cdcf: function(t, a, i) {
			var n = i("24fb");
			a = n(!1), a.push([t.i,
				".widthWX[data-v-003cd546]{width:%?550?%}.tab-bar[data-v-003cd546]{width:100%;position:fixed;top:0;left:0;background-color:#fff;z-index:10;padding-top:calc(0px + %?18?%)}.kuozhan-ul[data-v-003cd546]{padding:calc(0px + %?135?%) %?20?% %?200?%}.kuozhan-ul .kuozhan-li[data-v-003cd546]{padding:%?20?%;background-color:#fff;border-radius:%?10?%;margin-bottom:%?20?%;box-shadow:%?5?% %?5?% %?8?% #ccc}.kuozhan-ul .kuozhan-li .kuozhan-title[data-v-003cd546]{display:flex;justify-content:space-between;margin-bottom:%?20?%}.kuozhan-ul .kuozhan-li .kuozhan-title uni-view[data-v-003cd546]:first-child{font-size:%?30?%;font-weight:700}.kuozhan-ul .kuozhan-li .kuozhan-title uni-view[data-v-003cd546]:last-child{font-size:%?30?%;color:#409eff;text-decoration:underline}.kuozhan-ul .kuozhan-li .kuozhan-zuozhe[data-v-003cd546]{display:flex;justify-content:space-between;margin-bottom:%?20?%}.kuozhan-ul .kuozhan-li .kuozhan-zuozhe uni-view[data-v-003cd546]:first-child{color:#333}.kuozhan-ul .kuozhan-li .kuozhan-zuozhe uni-view[data-v-003cd546]:last-child{color:#999}.kuozhan-ul .kuozhan-li .kuozhan-content[data-v-003cd546]{height:%?110?%;overflow:hidden;color:#333;font-size:%?26?%;box-shadow:%?0?% %?0?% %?5?% %?2?% #ccc inset;border-radius:%?10?%;padding:%?20?%}.kuozhan-ul .kuozhan-li .kuozhan-content-child[data-v-003cd546]{color:#333;font-size:%?26?%;box-shadow:%?0?% %?0?% %?5?% %?2?% #409eff inset;border-radius:%?10?%;padding:%?20?%}.box-null[data-v-003cd546]{width:100%;text-align:center}.box-null .bg[data-v-003cd546]{background-color:#fff;border-radius:%?10?%;padding:%?80?%;box-shadow:%?5?% %?5?% %?8?% #ccc;font-size:%?40?%;font-weight:700;color:#333}.box-null .bg uni-image[data-v-003cd546]{height:%?350?%}",
				""
			]), t.exports = a
		},
		d80a: function(t, a, i) {
			"use strict";
			i.r(a);
			var n = i("a37d"),
				e = i.n(n);
			for (var o in n)["default"].indexOf(o) < 0 && function(t) {
				i.d(a, t, (function() {
					return n[t]
				}))
			}(o);
			a["default"] = e.a
		},
		d99f: function(t, a, i) {
			"use strict";
			i.r(a);
			var n = i("13ed"),
				e = i("0b64");
			for (var o in e)["default"].indexOf(o) < 0 && function(t) {
				i.d(a, t, (function() {
					return e[t]
				}))
			}(o);
			i("b828");
			var s = i("f0c5"),
				c = Object(s["a"])(e["default"], n["b"], n["c"], !1, null, "6c32742c", null, !1, n["a"],
					void 0);
			a["default"] = c.exports
		},
		e037: function(t, a, i) {
			"use strict";
			i.d(a, "b", (function() {
				return n
			})), i.d(a, "c", (function() {
				return e
			})), i.d(a, "a", (function() {}));
			var n = function() {
					var t = this,
						a = t.$createElement,
						i = t._self._c || a;
					return i("v-uni-view", [i("cu-custom", {
						attrs: {
							bgColor: "bg-white",
							isBack: !1
						}
					}, [i("template", {
						attrs: {
							slot: "backText"
						},
						slot: "backText"
					}), i("template", {
						attrs: {
							slot: "content"
						},
						slot: "content"
					}, [t._v("推荐文章")])], 2), i("v-uni-view", {
						staticClass: "app"
					}, [i("v-uni-view", {
						staticClass: "tuijian-ul"
					}, [t._l(t.newsList.data, (function(a, n) {
						return [i("v-uni-view", {
							key: n + "_0",
							staticClass: "tuijian-li",
							on: {
								click: function(i) {
									arguments[0] = i = t
										.$handleEvent(
										i), t.goUrl(a)
								}
							}
						}, [i("v-uni-image", {
							staticClass: "img",
							attrs: {
								src: a.img ||
									"/static/logo.png",
								mode: "aspectFill"
							}
						}), i("v-uni-view", {
							staticClass: "text"
						}, [t._v(t._s(a.title ||
							""))])], 1)]
					}))], 2), i("v-uni-view", {
						staticClass: "box-null"
					}, [0 == t.newsList.data.length ? i("v-uni-view", {
						staticClass: "bg"
					}, [i("v-uni-image", {
						attrs: {
							src: "/static/bg.jpg",
							mode: "aspectFit"
						}
					}), i("v-uni-view", [t._v("暂无数据")])], 1) : i("v-uni-view", {
						staticClass: "cu-load text-gray",
						class: -1 == t.newsList.isLoading ? "over" :
							"loading"
					})], 1)], 1)], 1)
				},
				e = []
		},
		e6ef: function(t, a, i) {
			"use strict";
			var n = i("e9c0"),
				e = i.n(n);
			e.a
		},
		e9c0: function(t, a, i) {
			var n = i("cdcf");
			n.__esModule && (n = n.default), "string" === typeof n && (n = [
				[t.i, n, ""]
			]), n.locals && (t.exports = n.locals);
			var e = i("4f06").default;
			e("3e55f9fa", n, !0, {
				sourceMap: !1,
				shadowMode: !1
			})
		},
		f148: function(t, a, i) {
			"use strict";
			var n = i("fe58"),
				e = i.n(n);
			e.a
		},
		f700: function(t, a, i) {
			"use strict";
			(function(t) {
				i("7a82"), Object.defineProperty(a, "__esModule", {
					value: !0
				}), a.default = void 0;
				var n = {
					data: function() {
						return {
							textData: []
						}
					},
					mounted: function() {
						this.getTongzhi()
					},
					methods: {
						getTongzhi: function() {
							var a = this;
							uni.showLoading({
								title: "加载中"
							}), t.callFunction({
								name: "gonggaoGet",
								data: {
									gonggao: this.text
								}
							}).then((function(t) {
								uni.hideLoading(), a.textData = t.result.data[0]
							}))
						},
						gofabus: function() {
							window.location.href = "static/h5/index.html"
						}
					}
				};
				a.default = n
			}).call(this, i("a9ff")["default"])
		},
		fe58: function(t, a, i) {
			var n = i("94ba");
			n.__esModule && (n = n.default), "string" === typeof n && (n = [
				[t.i, n, ""]
			]), n.locals && (t.exports = n.locals);
			var e = i("4f06").default;
			e("06bf4e5f", n, !0, {
				sourceMap: !1,
				shadowMode: !1
			})
		},
		fe86: function(t, a, i) {
			"use strict";
			i.d(a, "b", (function() {
				return n
			})), i.d(a, "c", (function() {
				return e
			})), i.d(a, "a", (function() {}));
			var n = function() {
					var t = this,
						a = t.$createElement,
						i = t._self._c || a;
					return i("v-uni-view", [i("cu-custom", {
						attrs: {
							bgColor: "bg-white",
							isBack: !1
						}
					}, [i("template", {
						attrs: {
							slot: "backText"
						},
						slot: "backText"
					}), i("template", {
						attrs: {
							slot: "content"
						},
						slot: "content"
					}, [t._v("公告")]), i("template", {
						attrs: {
							slot: "right"
						},
						slot: "right"
					}, [i("v-uni-view", {
						staticClass: "guanli",
						on: {
							click: function(a) {
								arguments[0] = a = t.$handleEvent(a), t
									.gofabus.apply(void 0, arguments)
							}
						}
					}, [i("v-uni-view", {
						staticClass: "cuIcon-group_fill"
					}), t._v("管理")], 1)], 1)], 2), i("v-uni-view", {
						staticClass: "title"
					}, [t._v("发布时间：" + t._s(t.textData.time || "暂无"))]), i("v-uni-view", {
						staticClass: "content"
					}, [i("v-uni-view", {
						domProps: {
							innerHTML: t._s(t.textData.tongzhi || "暂无")
						}
					})], 1)], 1)
				},
				e = []
		},
		ff2a: function(t, a, i) {
			"use strict";
			var n = i("66dd"),
				e = i.n(n);
			e.a
		}
	}
]);
